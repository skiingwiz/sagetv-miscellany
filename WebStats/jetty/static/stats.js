$(function(){
$("head").append("<link rel='stylesheet' href='/stats.css' type='text/css' media='screen'>");

$("fieldset > legend").click(function() {
  if ($(this).parent().children().length == 2) 
  {
    $(this).parent().find("div").toggle();
  }
  else
  {
    $(this).parent().wrapInner("<div>");
    $(this).appendTo($(this).parent().parent());
    $(this).parent().find("div").toggle();
  }

  $(this).parent().toggleClass("hidden");
});

$("fieldset > legend").click()

loadRecordingsByChannel('view_by_channel')
loadRecordingsByTime('view_by_time')
loadShowsByChannel('shows_by_channel')

$.getJSON( "/sage/stats_data.groovy", {
   "chart_name": "get_channels"
},
function( data ) {
   for(index in data) {
      var item = data[index]
      if(item.name != null && item.name.length > 0) {
	//alert(item.name)
      $('select.channel_list')
	   .append('<option>' + item.name + '</option>')
	   //.attr('value', item.name)
	   //.text(item.name)
      }
   }
});

});

function getValue(chartName, valueName, defaultValue)
{
   return $("#" + chartName + " form [name='" + valueName + "']").val() || defaultValue
}

function getCheck(chartName, valueName, defaultValue)
{
   return $("#" + chartName + " form [name='" + valueName + "']").is(":checked") || defaultValue
}

function getSubtitle(chartName, variables)
{
   var subtitle = getValue(chartName, 'subtitle', "")
   for(var v in variables) {
      subtitle = subtitle.replace("$" + v, variables[v])
   }
   return subtitle
}

function loadChart(chartName, loadVariables)
{
   loadVariables["chart_name"] = chartName
   var subtitle = getSubtitle(chartName, loadVariables)

   $("#" + chartName + " .subtitle").text(subtitle);
   $.getJSON( "/sage/stats_data.groovy", loadVariables,
   function( data ) {
      var chartDiv = $("#" + chartName + " div[title='chart']");
      chartDiv.html("<canvas width='600' height='400'>/canvas>");
      var ctx = chartDiv.children("canvas").get(0).getContext("2d");
      var chart = new Chart(ctx).Bar(data, {
      legendTemplate : "<table><% for (var i=0; i<datasets.length; i++){%><tr><td><div style=\"width:20px;height:20px;background-color:<%=datasets[i].fillColor%>\"></div></td><td><%if(datasets[i].label){%><%=datasets[i].label%><%}%></td></tr><%}%></table>"

      });
      $("#" + chartName + " [title='legend']").html(chart.generateLegend());
   });
}

function loadRecordingsByChannel(chartName)
{
   var num_divs = getValue(chartName, 'num_divs', 10)
   var num_months = getValue(chartName, 'num_months', 12)
   var show_other = getCheck(chartName, 'show_other', false)

   loadChart(chartName, {
      "num_divs": num_divs,
      "num_months": num_months,
      "show_other": show_other
   })
}

function loadRecordingsByTime(chartName)
{
   var num_divs = getValue(chartName, 'num_divs', 8)
   var num_months = getValue(chartName, 'num_months', 12)

   loadChart(chartName, {
      "num_divs": num_divs,
      "num_months": num_months
   })
}

function loadShowsByChannel(chartName)
{
   var num_divs = getValue(chartName, 'num_divs', 10)
   var num_months = getValue(chartName, 'num_months', 12)
   var channel = getValue(chartName, 'channel', 'ALL')
   var show_other = getCheck(chartName, 'show_other', false)

   loadChart(chartName, {
      "num_divs": num_divs,
      "num_months": num_months,
      "channel":channel,
      "show_other": show_other
   })
}

