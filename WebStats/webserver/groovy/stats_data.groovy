import groovy.transform.Immutable
import groovy.transform.ToString
import sagex.api.AiringAPI;
import sagex.api.ChannelAPI;
import sagex.api.Global;
import sagex.api.ShowAPI;


void viewByChannel(def params) {
   Integer num_months = params.num_months?.toInteger() ?: 24
   Integer num_divs = params.num_divs?.toInteger() ?: 12
   def channelMap = new HashMap<String, Integer>();
   boolean show_other = params.show_other == "true"
   
   def cal = Calendar.instance
   cal.add(Calendar.MONTH, -num_months)
   def millis = Calendar.instance.timeInMillis - cal.timeInMillis
   
   for(airing in Global.GetRecentlyWatched(millis)) {
      def chan = AiringAPI.GetAiringChannelName(airing);
      def aff = ChannelAPI.GetChannelNetwork(airing);
      if(aff?.contains(" Affiliate")) {
          chan = aff.substring(0, aff.indexOf(" Affiliate"))
      }
      int count;
      if(channelMap.containsKey(chan)) {
         count = channelMap[chan] + 1;
      }  else {
         count = 1;
      }
   
      channelMap[chan] = count 
   }
   
   //TODO remove empty keys from Map??
   channelMap = channelMap.sort { a, b -> b.value <=> a.value }
   def otherCount = 0;
   if(show_other) {
      channelMap.drop(num_divs).each { k,v ->
         otherCount += v
      }
   }
   
   channelMap = channelMap.take(num_divs)
   if(show_other) {
      channelMap["Other"] = otherCount
   }

   def builder = new groovy.json.JsonBuilder()
   builder {
      labels channelMap.keySet()
      datasets([{
         label "Number of views"
         fillColor "rgba(151,187,205,0.5)"
         strokeColor "rgba(151,187,205,0.8)"
         highlightFill "rgba(151,187,205,0.75)"
         highlightStroke "rgba(151,187,205,1)"
         data channelMap.values()
      }])
   }
   println(builder.toString())
}

void viewByTime(def params) {
   Integer num_months = params.num_months?.toInteger() ?: 24
   Integer num_divs = params.num_divs?.toInteger() ?: 3
   def recordedMap = new HashMap<Integer, Integer>();
   def watchedMap = new HashMap<Integer, Integer>();
   
   def cal = Calendar.instance
   cal.add(Calendar.MONTH, -num_months)
   def millis = Calendar.instance.timeInMillis - cal.timeInMillis
   Integer hourBins = 24 / num_divs 
   def increment = {map, time ->
      def tod = Calendar.instance
      tod.setTimeInMillis(time);
      Integer bin = tod.get(Calendar.HOUR_OF_DAY) % hourBins
      int count;
      if(map.containsKey(bin)) {
         count = map[bin] + 1;
      }  else {
         count = 1;
      }
   
      map[bin] = count 
   }
   for(airing in Global.GetRecentlyWatched(millis)) {
      long recStartTime = AiringAPI.GetAiringStartTime(airing);
      increment(recordedMap, recStartTime);
      long watchStartTime = AiringAPI.GetRealWatchedStartTime(airing)
      increment(watchedMap, watchStartTime)
   }

   recordedMap = recordedMap.sort()
   watchedMap = watchedMap.sort()

   def builder = new groovy.json.JsonBuilder()
   builder {
      labels recordedMap.keySet().collect { (it.toInteger() * num_divs) + ":00 - " + ((it.toInteger() + 1) * num_divs - 1) + ":59"}
      datasets([{
         label "Time Recorded"
         fillColor "rgba(151,187,205,0.5)"
         strokeColor "rgba(151,187,205,0.8)"
         highlightFill "rgba(151,187,205,0.75)"
         highlightStroke "rgba(151,187,205,1)"
         data recordedMap.values()
       },{
         label "Time Watched"
         fillColor "rgba(220,220,220,0.5)"
         strokeColor "rgba(220,220,220,0.8)"
         highlightFill "rgba(220,220,220,0.75)"
         highlightStroke "rgba(220,220,220,1)"
         data watchedMap.values()
      }])
   }
   println(builder.toString())
}

void showsByChannel(def params) {
   Integer num_months = params.num_months?.toInteger() ?: 24
   Integer num_divs = params.num_divs?.toInteger() ?: 3
   String channel_name = params.channel ?: "ALL"
   boolean show_other = params.show_other == "true"

   def showMap = new HashMap<String, Integer>();
   
   def cal = Calendar.instance
   cal.add(Calendar.MONTH, -num_months)
   def millis = Calendar.instance.timeInMillis - cal.timeInMillis

   def increment = {map, show ->
      int count;
      if(map.containsKey(show)) {
         count = map[show] + 1;
      }  else {
         count = 1;
      }
   
      map[show] = count 
   }

   for(airing in Global.GetRecentlyWatched(millis)) {
      if(channel_name == "ALL" || AiringAPI.GetAiringChannelName(airing) == channel_name) {
         String show = ShowAPI.GetShowTitle(airing)
         increment(showMap, show)
      }
   }

   showMap = showMap.sort { a, b -> b.value <=> a.value }
   def otherCount = 0;
   if(show_other) {
      showMap.drop(num_divs).each { k,v ->
         otherCount += v
      }
   }
   
   showMap = showMap.take(num_divs)
   if(show_other) {
      showMap["Other"] = otherCount
   }

   def builder = new groovy.json.JsonBuilder()
   builder {
      labels showMap.keySet()
      datasets([{
         label "Show Name"
         fillColor "rgba(151,187,205,0.5)"
         strokeColor "rgba(151,187,205,0.8)"
         highlightFill "rgba(151,187,205,0.75)"
         highlightStroke "rgba(151,187,205,1)"
         data showMap.values()
      }])
   }
   println(builder.toString())
}

@Immutable
@ToString
class Channel {
     String name
     String network
}

void getChannels() {
   def airings = Global.GetRecentlyWatched(Long.MAX_VALUE)
   chanList = airings.collect { chan ->
       new Channel(ChannelAPI.GetChannelName(chan),
          ChannelAPI.GetChannelNetwork(chan)
       )
   }
   chanList = chanList.unique()
   chanList.sort { a, b -> a.name <=> b.name }
   def builder = new groovy.json.JsonBuilder()
   builder chanList
   println(builder.toString())
}

String chartName = params.chart_name
switch(chartName)
{
  case "view_by_channel":
      viewByChannel(params)
      break
  case "view_by_time":
      viewByTime(params)
      break
  case "shows_by_channel":
      showsByChannel(params)
      break
  case "get_channels":
      getChannels()
      break
}
