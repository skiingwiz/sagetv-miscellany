<%
//request.setAttribute("title", "SageTV Stats")
request.setAttribute("pageTitle", "SageTV Stats")
def scripts = new ArrayList<String>();
scripts += "/chartjs/Chart.min.js"
scripts += "/stats.js"

request.setAttribute('scripts', scripts);
%>

<@ webserver/templates/header.gsp @>

<div class="column left">
<div class='panel' id='view_by_channel'>
   <h1>Recordings by Channel</h1>
   <h2 class="subtitle"></h2>
   <div title='chart'></div> 

   <form onsubmit="javascript:loadRecordingsByChannel('view_by_channel')" action='javascript:void(0)'>
   <fieldset class="customize">
      <legend>Customize this chart</legend>
      Show 
      <input type="number" min="3" max="50" step="1" 
             size="7"
             required="required" placeholder="Channels" name="num_divs" /> Top Channels <br/>
      Over the last 
      <input type="number" min="1" step="1" 
             size="7"
             required="required" placeholder="Months" name="num_months" /> Months<br/>
      <input type="checkbox" name="show_other">Show 'Other' Value</input><br/>
      <input type="submit" value="Update" />
      <input type="hidden" name="subtitle" value="Over \$num_months Months" />
   </fieldset>
   </form>
</div>

<div class='panel' id='shows_by_channel'>
   <h1>Shows by Channel</h1>
   <h2 class="subtitle"></h2>
   <div title='chart'></div> 

   <form onsubmit="javascript:loadShowsByChannel('shows_by_channel')" action='javascript:void(0)'>
   <fieldset class="customize">
      <legend>Customize this chart</legend>
      Show 
      <input type="number" min="3" max="50" step="1" 
             size="7"
             required="required" placeholder="Shows" name="num_divs" /> Top Shows <br/>
      Over the last 
      <input type="number" min="1" step="1" 
             size="7"
             required="required" placeholder="Months" name="num_months" /> Months<br/>
      Channel
      <select name="channel" class="channel_list">
         <option value="ALL" selected="selected">All Channels</option>
      </select> <br/>
      <input type="checkbox" name="show_other">Show 'Other' Value</input><br/>
      <input type="submit" value="Update" />
      <input type="hidden" name="subtitle" value="Over \$num_months Months" />
   </fieldset>
   </form>
</div>

</div>
<div class="column right">
<div class='panel' id='view_by_time'>
   <h1>Recordings by Time of Day</h1>
   <h2 class="subtitle"></h2>
   <div title='chart'></div> 
   <div title='legend'></div> 

   <form onsubmit="javascript:loadRecordingsByTime('view_by_time')" action='javascript:void(0)'>
   <fieldset class="customize">
      <legend>Customize this chart</legend>
      Show every  
      <select name="num_divs">
         <option value="2">2</option>
         <option value="3" selected="selected">3</option>
         <option value="4">4</option>
         <option value="6">6</option>
      </select>
 Hours <br/>
      Over the last 
      <input type="number" min="1" step="1" 
             size="7"
             required="required" placeholder="Months" name="num_months" /> Months<br/>
      <input type="submit" value="Update" />
      <input type="hidden" name="subtitle" value="Over \$num_months Months" />
   </fieldset>
   </form>
</div>
</div>
</body>
</html>
<!-- @ webserver/templates/footer.gsp @ -->


