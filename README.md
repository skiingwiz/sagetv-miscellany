# README #

### What is this repository for? ###

This repository is a collection of small plugins for [Sage TV](http://www.sage.tv)

### License ###
All code and content made available under the terms of the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)